const cron = require("node-cron");
const cleanUpSchedules = require("./services/cleanUpSchedulesController");

//Agendamento da limpeza, o schedule a seguir é nativo do cron
cron.schedule("0 0 * * * *", async()=>{
    try {
        await cleanUpSchedules();
        console.log("Limpeza automática executada.")
    } catch (error) {
        console.error("Erro ao executar limpeza!")
    }
})