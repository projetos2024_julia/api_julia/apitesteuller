const connect = require("../db/connect");
module.exports = class scheduleController {
  static async createSchedule(req, res) {
    const { dateStart, dateEnd, days, user, classroom, timeStart, timeEnd } =
      req.body;

    if (
      !dateStart ||
      !dateEnd ||
      !days ||
      !user ||
      !classroom ||
      !timeStart ||
      !timeEnd
    ) {
      return res
        .status(400)
        .json({ error: "Todos os campos devem ser preenchidos" });
    } //fim do if

    const dayString = days.map((day) => `${day}`).join(", ");

    //Verificando se já não existe uma reserva

    try {
      //Verificação se vai sobregarregar a requisição
      const overlapQuery = `SELECT * FROM schedule
        WHERE classroom = '${classroom}'
        AND (
            (dateStart<='${dateEnd}' AND dateEnd >='${dateStart}')
        ) AND (
            (timeStart<='${timeEnd}' AND timeEnd >='${timeStart}')
        ) AND (
            (days LIKE '%Seg%' AND '${dayString}' LIKE '%Seg%')OR
            (days LIKE '%Ter%' AND '${dayString}' LIKE '%Ter%')OR
            (days LIKE '%Qua%' AND '${dayString}' LIKE '%Qua%')OR
            (days LIKE '%Qui%' AND '${dayString}' LIKE '%Qui%')OR
            (days LIKE '%Sex%' AND '${dayString}' LIKE '%Sex%')OR
            (days LIKE '%Sab%' AND '${dayString}' LIKE '%Sab%')
        );
        
        `;

      connect.query(overlapQuery, function (err, results) {
        if (err) {
            console.error("Erro ao executar a consulta:", err);
          res.status(500).json({ error: "Erro ao verificar o agendamento." });
        } //fim do if verificar agendamento

        //Se houver resultado à consulta, já existe agendamento
        if (results.length > 0) {
          return res
            .status(400)
            .json({ error: "Sala ocupada para os mesmos dias e horários" });
        } //Fim do if sala ocupada

        //Caso a query não retorne nada inserimos na tabela

        const insertQuery = ` INSERT INTO schedule (
            dateStart, 
            dateEnd, 
            days, 
            user, 
            classroom, 
            timeStart, 
            timeEnd
        )VALUES(
            '${dateStart}',
            '${dateEnd}',
            '${days}',
            '${user}',
            '${classroom}',
            '${timeStart}',
            '${timeEnd}'
        )
            ;`;

        connect.query(insertQuery, function (err) {
          if (err) {
            console.error("Erro ao executar a consulta:", err);
            res.status(500).json({ error: "Erro ao cadastrar agendamento" });
          } //fim do if
          
          return res
            .status(201)
            .json({ message: "Agendamento cadastrado com sucesso!" });
        });

      }); 

    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno no servidor" });
    }
  } //Fim do createSchedule
}; //Fim da scheduleController
